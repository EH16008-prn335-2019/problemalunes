/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.edu.sv.ingenieria.prn335.boundary;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import occ.edu.sv.ingenieria.prn335.controller.Cine;
import occ.edu.sv.ingenieria.prn335.entity.Pelicula;

/**
 *
 * @author armando
 */
@WebServlet(name = "NewServlet", urlPatterns = {"/NewServlet"})
public class NewServlet extends HttpServlet {

    @Inject
    public Cine cine;
    Pelicula pelicula = new Pelicula();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Pelicula Servlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FrmServlet at /guia01-2019</h1>");
            out.println("<button onclick=\"location.href='index.html'\">Volver al Index</button>");
            out.println("</br>");
            out.println("<h2>Agregado</h2>");
            String btnAccionado = request.getParameter("btnAccionado");
            if (null == btnAccionado) {
                out.println("<h2>No se selecionó nada</h2> ");
            } else {
                int idPelicula = Integer.parseInt(request.getParameter("id"));
                String titulo = request.getParameter("txtTitulo");
                String duracion = request.getParameter("txtDuracion");
                String director = request.getParameter("txtDirector");
                String genero = request.getParameter("txtGenero");
                String fecha = request.getParameter("txtFechaEstreno");
                LocalDate fechaEstreno = LocalDate.parse(fecha);
                char[] clas = request.getParameter("txtClasificacion").toCharArray();
                char clasificacion = clas[0];
                String sinopsis = request.getParameter("txtSinopsis");
                switch (btnAccionado) {
                    case "Agregar":
//                        Pelicula e = new Pelicula(idPelicula, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);
//                        cine.getListaPeliculas().add(e);
                        cine.agregarPelicula(idPelicula, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);
                        out.println("<table border=\"1\">");
                        out.println(" <thead>");
                        out.println(" <tr>");
                        out.println("<th>ID</th>");
                        out.println("<th>Película</th>");
                        out.println("<th>Duración</th>");
                        out.println("<th>Directo</th>");
                        out.println("<th>Género</th>");
                        out.println("<th>Fecha</th>");
                        out.println("<th>Clasificacion</th>");
                        out.println("<th>Sinopsis</th>");
                        out.println("</tr>");
                        out.println(" </thead>");
                        out.println("  <tbody>");
                        for (int i = 0; i < cine.getListaPeliculas().size(); i++) {
                            out.println("\n");
                            out.println(" <tr>");
                            out.println(" <td>" + cine.getListaPeliculas().get(i).getIdPelicula() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(i).getTitulo() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(i).getDuracion() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(i).getDirector() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(i).getGenero() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(i).getFechaEstreno() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(i).getClasificacion() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(i).getSinopsis() + "</td>");
                            out.println(" </tr>");
                        }
                        out.println(" </tbody>");
                        out.println("  </table>");
                        break;

                    case "Modificar":
//                        for (int i = 0; i < cine.getListaPeliculas().size(); i++) {
//                            if (idPelicula == i) {
//                                Pelicula a = new Pelicula(i, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);
//                                cine.getListaPeliculas().set(i - 1, a);
//                            } else {
//                                System.out.println("No se encontro ese ID");
//                            }
//                        }
                        cine.modificarPelicula(idPelicula, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);

                        out.println("<table border=\"1\">");
                        out.println(" <thead>");
                        out.println(" <tr>");
                        out.println("<th>ID</th>");
                        out.println("<th>Película</th>");
                        out.println("<th>Duración</th>");
                        out.println("<th>Directo</th>");
                        out.println("<th>Género</th>");
                        out.println("<th>Fecha</th>");
                        out.println("<th>Clasificacion</th>");
                        out.println("<th>Sinopsis</th>");
                        out.println("</tr>");
                        out.println(" </thead>");
                        out.println("  <tbody>");
                        for (int j = 0; j < cine.getListaPeliculas().size(); j++) {
                            out.println("\n");
                            out.println(" <tr>");
                            out.println(" <td>" + cine.getListaPeliculas().get(j).getIdPelicula() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(j).getTitulo() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(j).getDuracion() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(j).getDirector() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(j).getGenero() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(j).getFechaEstreno() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(j).getClasificacion() + "</td>");
                            out.println(" <td>" + cine.getListaPeliculas().get(j).getSinopsis() + "</td>");
                            out.println(" </tr>");
                        }

                        out.println(" </tbody>");
                        out.println("  </table>");
                        break;

                    default:
                        break;
                }
            }
            out.println("</body>");
            out.println("</html>");
        } catch (Exception ex) {
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
