/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.edu.sv.ingenieria.prn335.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import occ.edu.sv.ingenieria.prn335.entity.Pelicula;

/**
 *
 * @author armando
 */
public class Cine {
       ArrayList<Pelicula> listaPeliculas = new ArrayList<>();

   public Cine() {
        listaPeliculas.add(new Pelicula(1, "Blade Runner 2049", "163 minutos", "Dennis Villeneuve", "Ciencia Ficcion", LocalDate.of(2017, 10, 3), 'C', "Un androide tiene una crisis existencial"));
        listaPeliculas.add(new Pelicula(2, "A Quiet Place", "91 minutos", "John Krasinski", "Horror", LocalDate.of(2018, 3, 9), 'D', "Da miedo"));
        listaPeliculas.add(new Pelicula(3, "The Incredibles II", "120 minutos", "Brad Bird", "Acción", LocalDate.of(2016, 6, 15), 'A', "Un esposo celoso del exito de us esposa"));
        listaPeliculas.add(new Pelicula(4, "Avengers: Infinity War", "149 minutos", "Hermanos Russo", "Acción", LocalDate.of(2018, 4, 23), 'B', "Thanos encuentra el one pice"));
        listaPeliculas.add(new Pelicula(5, "Dunkirk", "106 minutos", "Christopher Nolan", "Guerra", LocalDate.of(2017, 7, 21), 'C', "Los ingleses se van de francia en barquitos"));
    }

  

    public ArrayList<Pelicula> getListaPeliculas() {
        return listaPeliculas;
    }

    public Pelicula agregarPelicula(int idPelicula, String titulo, String duracion,
String director, String genero, LocalDate fechaEstreno, char clasificacion, String
sinopsis){
        Pelicula e = new Pelicula(idPelicula, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);
        listaPeliculas.add(e);
//        try {
//            char clasiE='E';
//            if (clasificacion==clasiE) {
//                System.out.println("No se pueden ingresar clasificatorias 'E'");
//            }else{
//                LocalDate fechaActual=LocalDate.now();
//                if (fechaActual.isBefore(fechaEstreno)) {
//                    System.out.println("No se puede ingresar con fechas anteriores");
//            }
//            }
//        } catch (Exception e) {
//        }
    return e;
    
    }

public void modificarPelicula(int id_pelicula, String titulo, String duracion, String
director, String genero, LocalDate fechaEstreno, char clasificacion, String sinopsis){
                        for (int i = 0; i < listaPeliculas.size(); i++) {
                            if (id_pelicula == i) {
                                Pelicula a = new Pelicula(i, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);
                                listaPeliculas.set(i - 1, a);
                            } else {
                                System.out.println("No se encontro ese ID");
                            }
                        }
          
    }
}

